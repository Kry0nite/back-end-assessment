<?php
class buah{
    public $nama;
    public $warna;

    public function __construct($nama, $warna){
        $this->nama = $nama;
        $this->warna = $warna;
    } 
    public function awal(){
        echo "Buah {$this->nama} dan warnanya adalah {$this->warna}";
    }
}
class  Mangga extends buah{
    public function pesan(){
        echo "Ini adalah ";
    }
}
    $mangga = new mangga("Mangga","Hijau");
    $mangga->pesan();
    $mangga->awal();
?>
