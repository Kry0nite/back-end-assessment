<?php
class buah{
    public $nama;
    public $warna;

    function set_nama($nama){
        $this->nama = $nama;
    }
    function get_nama(){
        return $this->nama;
    }
    function set_warna($warna){
        $this->warna = $warna;
    }
    function get_warna(){
        return $this->warna;
    }
}

$mangga     = new buah();
$manggis    = new buah();
$pisang     = new buah();
$pepaya     = new buah();

$mangga-> set_nama('Mangga');
$manggis-> set_nama('Manggis');
$pisang-> set_nama('Pisang');
$pepaya-> set_nama('Pepaya');

$mangga-> set_warna('Hijau');
$manggis-> set_warna('Ungu');
$pisang-> set_warna('Kuning');
$pepaya-> set_warna('Hijau Kekuningan');


echo "Nama  :". $mangga->get_nama();
echo "<br>";
echo "Warna :". $mangga->get_warna();
?>
