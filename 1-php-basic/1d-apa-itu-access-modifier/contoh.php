<?php
		Class induk
		{
			public $dataPublic="Ini data Public";
			private $dataPrivate="Ini data Private";
			protected $dataProtected="Ini data Protected";

			public function PublicFunction(){
				echo "Ini Fungsi Public";
			}

			private function PrivateFunction(){
				echo "Ini Fungsi Private";
			}

			protected function ProtectedFunction(){
				echo "Ini Fungsi Protected";
			}

		}
		class anak extends induk
		{

			public function Property(){
				echo "dataPublic = ".$this->dataPublic."<br/>";
				echo "dataProtected = ".$this->dataProtected."<br/>";

			}

			public function RemakeProtectedFunction(){
				$this->ProtectedFunction();
			}

		}

		$data=New anak();

		echo "<hr/>dataPublic = ".$data->dataPublic." <br><hr/>";
		
		$data->Property();
		
		echo "<hr>";
		$data->PublicFunction();

		echo "<br/>";
		$data->RemakeProtectedFunction();

?>
